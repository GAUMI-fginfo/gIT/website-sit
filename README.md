# Website for studentische Informatiktage Goettingen

This is the codebase to render content as provided by the
[sit-events repository](https://git.fg.informatik.uni-goettingen.de/fachgruppe/sit-events).
