FROM node:alpine
RUN ip a
RUN apk --no-cache add git bash
RUN mkdir -p /app && chown node /app
USER node
RUN git clone --recursive --depth 1 https://gitlab.gwdg.de/GAUMI-fginfo/website-sit.git /app
RUN cd /app/events && git pull && cd /app
WORKDIR /app
RUN npm install
RUN cd /app/events && git config pull.rebase true && git pull
EXPOSE 3000
CMD npm start
